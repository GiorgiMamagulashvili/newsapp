package com.example.newsapplication.model

data class SportNewsResponse(
    val articles: List<Article>,
    val status: String,
    val totalResults: Int
)