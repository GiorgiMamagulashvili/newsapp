package com.example.newsapplication.api

import com.example.newsapplication.model.Article
import com.example.newsapplication.model.SportNewsResponse
import com.example.newsapplication.other.Constants.API_KEY
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface SportNewsAPI {

    @GET("v2/top-headlines")
    suspend fun getSportNews(
        @Query("country")
        countryCode: String = "gb",
        @Query("category")
        category: String = "sport",
        @Query("page")
        pageNum: Int = 1,
        @Query("apiKey")
        apiKey: String = API_KEY
    ):Response<SportNewsResponse>

    @GET("v2/everything")
    suspend fun searchSportNews(
        @Query("q")
        searchNews:String,
        @Query("page")
        pageNum: Int = 1,
        @Query("apiKey")
        apiKey: String = API_KEY
    ):Response<SportNewsResponse>
}