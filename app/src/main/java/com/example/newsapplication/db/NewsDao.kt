package com.example.newsapplication.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.newsapplication.model.Article

@Dao
interface NewsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertNews(article: Article):Long

    @Delete
    suspend fun deleteNews(article: Article)

    @Query("SELECT * FROM article_table")
    fun getAllNews(): LiveData<List<Article>>
}