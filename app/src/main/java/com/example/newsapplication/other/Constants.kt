package com.example.newsapplication.other

object Constants {

    const val API_KEY = "22c65d396e9a4933a40c66cabd2f63f6"

    const val BASE_URL = "https://newsapi.org/"

    const val DATABASE_NAME = "news_database"
}