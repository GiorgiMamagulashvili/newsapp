package com.example.newsapplication.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.newsapplication.databinding.NewsRowItemBinding
import com.example.newsapplication.model.Article

class NewsAdapter : RecyclerView.Adapter<NewsAdapter.NewsViewHolder>() {

    inner class NewsViewHolder(val binding: NewsRowItemBinding) :
        RecyclerView.ViewHolder(binding.root)

    private val diffUtilCallBack = object : DiffUtil.ItemCallback<Article>() {
        override fun areItemsTheSame(oldItem: Article, newItem: Article): Boolean {
            return oldItem.url == newItem.url
        }

        override fun areContentsTheSame(oldItem: Article, newItem: Article): Boolean {
            return oldItem == newItem
        }

    }
    val differ = AsyncListDiffer(this, diffUtilCallBack)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        return NewsViewHolder(
            NewsRowItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        val news = differ.currentList[position]
        holder.binding.apply {
            tvDescription.text = news.description
            tvPublishedAt.text = news.publishedAt
            tvSource.text = news.source.name
            tvTitle.text = news.title
            holder.itemView.setOnClickListener{
                onItemSelected?.let { it(news) }
            }
        }
        holder.itemView.apply {
            Glide.with(this).load(news.urlToImage).into(holder.binding.ivArticleImage)
        }
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    private var onItemSelected:((Article) -> Unit)? = null

    fun setOnItemSelected(listener:(Article) -> Unit){
        onItemSelected = listener
    }
}