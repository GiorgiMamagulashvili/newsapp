package com.example.newsapplication.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.newsapplication.R
import com.example.newsapplication.databinding.ActivityMainBinding
import com.example.newsapplication.ui.fragment.NewsFragment
import com.example.newsapplication.ui.fragment.SavedNewsFragment
import com.example.newsapplication.ui.fragment.SearchFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    lateinit var binding:ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val newsFragment = NewsFragment()
        val searchFragment = SearchFragment()
        val savedNewsFragment = SavedNewsFragment()

        binding.bottomNavigationView.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.menu_news -> setCurrentFragment(newsFragment)
                R.id.menu_search -> setCurrentFragment(searchFragment)
                R.id.menu_saved -> setCurrentFragment(savedNewsFragment)
            }
            true
        }
    }

    private fun setCurrentFragment(fragment:Fragment){
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.navHostFragment,fragment)
            commit()
        }
    }
}