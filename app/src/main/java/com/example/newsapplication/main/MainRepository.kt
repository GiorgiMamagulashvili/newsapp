package com.example.newsapplication.main

import com.example.newsapplication.api.SportNewsAPI
import com.example.newsapplication.db.NewsDao
import com.example.newsapplication.model.Article
import javax.inject.Inject

class MainRepository @Inject constructor(
    private val api:SportNewsAPI,
    private val dao: NewsDao
) {
    suspend fun getSportNews(countryCode:String,category:String,pageNum:Int) = api.getSportNews(countryCode,category,pageNum)

    suspend fun searchNews(searchNews:String,pageNum: Int) = api.searchSportNews(searchNews,pageNum)

    suspend fun insertNews(article: Article) = dao.insertNews(article)

    suspend fun deleteNews(article: Article) = dao.deleteNews(article)

    fun getAllNews() = dao.getAllNews()
}