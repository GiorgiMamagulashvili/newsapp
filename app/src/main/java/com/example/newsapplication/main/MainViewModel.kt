package com.example.newsapplication.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.newsapplication.model.Article
import com.example.newsapplication.model.SportNewsResponse
import com.example.newsapplication.other.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val repository: MainRepository
) : ViewModel() {
    val sportNews: MutableLiveData<Resource<SportNewsResponse>> = MutableLiveData()
    var sportNewsPageNumber = 1

    val searchNews: MutableLiveData<Resource<SportNewsResponse>> = MutableLiveData()
    var searchNewsPageNumber = 1

    init {
        getSportNews("gb", "sport")
    }

    fun getSportNews(countryCode: String, category: String) = viewModelScope.launch {
        sportNews.postValue(Resource.Loading())
        val response = repository.getSportNews(countryCode, category, sportNewsPageNumber)
        sportNews.postValue(handleSportNewsResponse(response))

    }

    fun searchNews(search: String) = viewModelScope.launch {
        searchNews.postValue(Resource.Loading())
        val response = repository.searchNews(search, searchNewsPageNumber)
        searchNews.postValue(handleSearchNews(response))

    }

    private fun handleSportNewsResponse(response: Response<SportNewsResponse>): Resource<SportNewsResponse> {
        if (response.isSuccessful) {
            response.body()?.let { result ->
                return Resource.Success(result)
            }
        }
        return Resource.Error(response.message())
    }

    private fun handleSearchNews(response: Response<SportNewsResponse>): Resource<SportNewsResponse> {
        if (response.isSuccessful) {
            response.body()?.let { result ->
                return Resource.Success(result)
            }
        }
        return Resource.Error(response.message())
    }

    fun saveNews(article: Article) = viewModelScope.launch(Dispatchers.IO) {
        repository.insertNews(article)
    }

    fun deleteNews(article: Article) = viewModelScope.launch {
        repository.deleteNews(article)
    }

    fun getSavedNews() = repository.getAllNews()
}