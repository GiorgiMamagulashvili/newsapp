package com.example.newsapplication.di

import android.content.Context
import androidx.room.Room
import com.example.newsapplication.api.SportNewsAPI
import com.example.newsapplication.db.NewsDatabase
import com.example.newsapplication.other.Constants.BASE_URL
import com.example.newsapplication.other.Constants.DATABASE_NAME
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    fun provideApi(): SportNewsAPI = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .client(OkHttpClient().newBuilder().addInterceptor(HttpLoggingInterceptor()).build())
        .build()
        .create(SportNewsAPI::class.java)

    @Provides
    fun provideDatabase(
        @ApplicationContext app:Context
    ) = Room.databaseBuilder(
        app,
        NewsDatabase::class.java,
        DATABASE_NAME
    ).build()

    @Provides
    fun provideNewsDao(db:NewsDatabase) = db.getNewsDao()
}